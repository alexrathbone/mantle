#include "Mantle/Utils/Memory.h"

#include <TlHelp32.h>
#include <Psapi.h>

namespace Mantle
{
	std::vector<std::string> GetLoadedModulesNames()
	{
		std::vector<std::string> modules;

		HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, GetCurrentProcessId());
		
		if (snapshot == INVALID_HANDLE_VALUE)
			return modules;

		MODULEENTRY32 moduleEntry;
		moduleEntry.dwSize = sizeof(MODULEENTRY32);

		if (Module32First(snapshot, &moduleEntry))
		{
			do
			{
				modules.push_back(std::string(moduleEntry.szModule));
			} while (Module32Next(snapshot, &moduleEntry));
		}

		CloseHandle(snapshot);

		return modules;
	}

	std::vector<Mantle::ModuleInfo> GetLoadedModules()
	{
		std::vector<Mantle::ModuleInfo> modules;
		Mantle::ModuleInfo info;

		HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, GetCurrentProcessId());

		if (snapshot == INVALID_HANDLE_VALUE)
			return modules;

		MODULEENTRY32 moduleEntry;
		moduleEntry.dwSize = sizeof(MODULEENTRY32);

		if (Module32First(snapshot, &moduleEntry))
		{
			do
			{
				info.name = std::string(moduleEntry.szModule);
				info.path = std::string(moduleEntry.szExePath);
				info.baseAddress = (uintptr_t) moduleEntry.modBaseAddr;
				info.size = (uintptr_t) moduleEntry.modBaseSize;
				modules.push_back(info);
			} while (Module32Next(snapshot, &moduleEntry));
		}

		CloseHandle(snapshot);

		return modules;
	}

	Mantle::ModuleHandle GetModuleByName(const std::string& name)
	{
		if (name == "")
			return GetModuleHandleA(NULL);
		else
			return GetModuleHandleA(name.c_str());
	}

	Mantle::ModuleInfo GetModuleInfo(Mantle::ModuleHandle module)
	{
		Mantle::ModuleInfo info;
		info.name = "";
		info.path = "";
		info.baseAddress = 0;
		info.size = 0;

		MODULEINFO realInfo;
		if (GetModuleInformation(GetCurrentProcess(), (HMODULE) module, &realInfo, sizeof(MODULEINFO)) != FALSE)
		{
			info.baseAddress = (uintptr_t) realInfo.lpBaseOfDll;
			info.size = (uintptr_t) realInfo.SizeOfImage;

			CHAR buffer[MAX_PATH];
			GetModuleBaseNameA(GetCurrentProcess(), (HMODULE) module, &buffer[0], MAX_PATH);
			info.name = std::string(buffer);
			GetModuleFileNameA((HMODULE) module, &buffer[0], MAX_PATH);
			info.path = std::string(buffer);
		}

		return info;
	}

	uintptr_t FollowRelativeAddress(uintptr_t address, uintptr_t trail)
	{
		return address + 0x4 + trail + (*(int32_t*) address);
	}
}
