#include "Mantle/Utils/DetourHook.h"

#include "Mantle/Exception.h"

#include <Windows.h>

#define JMP_PATCH_MINSIZE 14

namespace Mantle
{
	static std::vector<uint8_t> GenerateJmpPatch(uintptr_t destination, size_t patchSize)
	{
		/*
			push destinationLow
			mov dword ptr [rsp+4], destinationHigh
			ret
		 */
		std::vector<uint8_t> patch =
		{
			0x68, 0x00, 0x00, 0x00, 0x00,
			0xC7, 0x44, 0x24, 0x04, 0x00,
			0x00, 0x00, 0x00, 0xC3,
		};

		*(uint32_t*) &patch[1] = (uint32_t) destination;
		*(uint32_t*) &patch[9] = (uint32_t) (destination >> 32);

		patch.resize(patchSize, 0x90);

		return patch;
	}

	static std::vector<uint8_t> GenerateWrapper(uintptr_t callback, std::vector<uint8_t>& originalCode, uintptr_t returnAddress)
	{
		/*
			push rsp
			push rax
			push rcx
			push rdx
			push rbx
			push rbp
			push rsi
			push rdi
			push r8
			push r9
			push r10
			push r11
			push r12
			push r13
			push r14
			push r15
			pushfq

			mov rcx, rsp
			movabs rax, callback
			sub rsp, 20
			call rax
			add rsp, 20

			popfq
			pop r15
			pop r14
			pop r13
			pop r12
			pop r11
			pop r10
			pop r9
			pop r8
			pop rdi
			pop rsi
			pop rbp
			pop rbx
			pop rdx
			pop rcx
			pop rax
			pop rsp

			(original code)

			push returnAddressLow
			mov dword ptr[rsp+4], returnAddressHigh
			ret
		 */
		std::vector<uint8_t> wrapper =
		{
			0x54, 0x50, 0x51, 0x52, 0x53, 
			0x55, 0x56, 0x57, 0x41, 0x50, 
			0x41, 0x51, 0x41, 0x52, 0x41, 
			0x53, 0x41, 0x54, 0x41, 0x55, 
			0x41, 0x56, 0x41, 0x57, 0x9C, 
			0x48, 0x89, 0xE1, 0x48, 0xB8, 
			0x00, 0x00, 0x00, 0x00, 0x00, 
			0x00, 0x00, 0x00, 0x48, 0x83, 
			0xEC, 0x20, 0xFF, 0xD0, 0x48, 
			0x83, 0xC4, 0x20, 0x9D, 0x41, 
			0x5F, 0x41, 0x5E, 0x41, 0x5D, 
			0x41, 0x5C, 0x41, 0x5B, 0x41, 
			0x5A, 0x41, 0x59, 0x41, 0x58, 
			0x5F, 0x5E, 0x5D, 0x5B, 0x5A, 
			0x59, 0x58, 0x5C
		};

		*(uintptr_t*) &wrapper[30] = callback;

		// Append original code
		wrapper.insert(wrapper.end(), originalCode.begin(), originalCode.end());

		// Append the return
		std::vector<uint8_t> returnCode =
		{
			0x68, 0x00, 0x00, 0x00, 0x00,
			0xC7, 0x44, 0x24, 0x04, 0x00,
			0x00, 0x00, 0x00, 0xC3,
		};

		*(uint32_t*) &returnCode[1] = (uint32_t) returnAddress;
		*(uint32_t*) &returnCode[9] = (uint32_t) (returnAddress >> 32);

		wrapper.insert(wrapper.end(), returnCode.begin(), returnCode.end());

		return wrapper;
	}

	DetourHook::DetourHook(uintptr_t location, size_t patchSize, uintptr_t callback) :
		location(location),
		patchSize(patchSize)
	{
		if (patchSize < JMP_PATCH_MINSIZE)
			throw Mantle::Exception("patchSize did not meet JMP_PATCH_MINSIZE");

		// Backup original code
		backup.resize(patchSize);
		memcpy(&backup[0], (void*) location, patchSize);

		// Generate wrapper code and allocate space for it in the process
		auto wrapper = Mantle::GenerateWrapper(callback, backup, location + patchSize);
		wrapperSize = wrapper.size();
		wrapperLocation = (uintptr_t) VirtualAlloc(NULL, wrapperSize, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
		memcpy((void*) wrapperLocation, &wrapper[0], wrapperSize);

		// Generate jmp patch and overwrite existing code
		auto patch = Mantle::GenerateJmpPatch(wrapperLocation, patchSize);
		DWORD oldProtect, tmp;
		VirtualProtect((LPVOID) location, patchSize, PAGE_EXECUTE_READWRITE, &oldProtect);
		memcpy((void*) location, &patch[0], patchSize);
		VirtualProtect((LPVOID) location, patchSize, oldProtect, &tmp);
	}

	DetourHook::~DetourHook()
	{
		// Restore original code
		DWORD oldProtect, tmp;
		VirtualProtect((LPVOID) location, patchSize, PAGE_EXECUTE_READWRITE, &oldProtect);
		memcpy((void*) location, &backup[0], patchSize);
		VirtualProtect((LPVOID) location, patchSize, oldProtect, &tmp);

		// Free wrapper
		VirtualFree((LPVOID) wrapperLocation, wrapperSize, MEM_RELEASE);
	}
}
