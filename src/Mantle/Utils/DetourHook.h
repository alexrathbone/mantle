#pragma once

#include <vector>

#include <cstdint>

namespace Mantle
{
	struct CpuContext
	{
		uintptr_t rflags;
		uintptr_t r15;
		uintptr_t r14;
		uintptr_t r13;
		uintptr_t r12;
		uintptr_t r11;
		uintptr_t r10;
		uintptr_t r9;
		uintptr_t r8;
		uintptr_t rdi;
		uintptr_t rsi;
		uintptr_t rbp;
		uintptr_t rbx;
		uintptr_t rdx;
		uintptr_t rcx;
		uintptr_t rax;
		uintptr_t rsp;
	};

	class DetourHook
	{
	public:
		DetourHook(uintptr_t location, size_t patchSize, uintptr_t callback);
		~DetourHook();

	private:
		uintptr_t location;
		size_t patchSize;
		uintptr_t wrapperLocation;
		size_t wrapperSize;
		std::vector<uint8_t> backup;
	};
}