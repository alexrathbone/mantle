#include "Mantle/Utils/VtHookManager.h"

#include <Windows.h>

namespace Mantle
{
	VtHookManager::VtHookManager(uintptr_t instance, uint16_t backupSize) :
		instance(instance)
	{
		// Backup original virtual table
		originalVt = *(uintptr_t*) instance;

		// Create fake virtual table
		fakeVt.resize(backupSize * sizeof(uintptr_t), 0);
		memcpy(&fakeVt[0], *(uintptr_t**) instance, fakeVt.size());

		// Set instance to point to our fake virtual table instead
		DWORD oldProtect, tmp;
		VirtualProtect((LPVOID) instance, sizeof(uintptr_t), PAGE_READWRITE, &oldProtect);
		*(uintptr_t*) instance = (uintptr_t) &fakeVt[0];
		VirtualProtect((LPVOID) instance, sizeof(uintptr_t), oldProtect, &tmp);
	}

	VtHookManager::~VtHookManager()
	{
		// Set instance to point to original virtual table
		DWORD oldProtect, tmp;
		VirtualProtect((LPVOID) instance, sizeof(uintptr_t), PAGE_READWRITE, &oldProtect);
		*(uintptr_t*) instance = originalVt;
		VirtualProtect((LPVOID) instance, sizeof(uintptr_t), oldProtect, &tmp);
	}

	uintptr_t VtHookManager::hook(uint16_t index, uintptr_t callback)
	{
		fakeVt[index] = callback;
		return ((uintptr_t*) originalVt)[index];
	}

	void VtHookManager::unhook(uint16_t index)
	{
		fakeVt[index] = ((uintptr_t*) originalVt)[index];
	}
}
