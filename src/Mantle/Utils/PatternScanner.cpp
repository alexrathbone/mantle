#include "Mantle/Utils/PatternScanner.h"

#include "Mantle/Utils/Memory.h"

#include <Windows.h>
#include <Psapi.h>

#include <algorithm>

namespace Mantle
{
	uintptr_t FindPattern(std::string pattern, const std::string& moduleName)
	{
		Mantle::ModuleHandle module = Mantle::GetModuleByName(moduleName);
		Mantle::ModuleInfo moduleInfo = Mantle::GetModuleInfo(module);

		if (module == nullptr)
			return 0;

		// pattern ex: FF 3B AC ?? ?? EF 2A
		// should be converted into bytes: \xff\x3b\xac\x00\x00\xef\x2a
		//                       and mask: xxx??xx

		// Remove spaces from pattern
		pattern.erase(std::remove_if(pattern.begin(), pattern.end(),
			[](char c) { return isspace(c); }), pattern.end());

		std::transform(pattern.begin(), pattern.end(), pattern.begin(), ::tolower);

		if (pattern.length() % 2)
			return 0;

		size_t patternLength = pattern.length() / 2;
		uint8_t* patternBytes = new uint8_t[patternLength];
		uint8_t* patternMask = new uint8_t[patternLength];

		for (size_t i = 0, j = 0; i < pattern.length(); i += 2, ++j)
		{
			std::string byte = pattern.substr(i, 2);
			
			if (byte == "??")
			{
				patternBytes[j] = '\x00';
				patternMask[j] = '?';
			}
			else if (isalnum(byte[0]) && isalnum(byte[1]))
			{
				patternBytes[j] = (uint8_t) std::stoul(byte, 0, 16);
				patternMask[j] = 'x';
			}
			else
			{
				delete patternBytes;
				delete patternMask;
				return 0;
			}
		}

		// Search the specified module for the pattern
		uintptr_t patternAddress = 0;
		bool found;

		for (uintptr_t i = 0; i < moduleInfo.size - patternLength; ++i)
		{
			found = false;

			for (uintptr_t j = 0; j < patternLength; ++j)
			{
				if (patternMask[j] == '?')
					continue;

				uint8_t c = *(uint8_t*) (moduleInfo.baseAddress + i + j);

				if (c == patternBytes[j])
				{
					found = true;
				}
				else
				{
					found = false;
					break;
				}
			}

			if (found)
			{
				patternAddress = i;
				break;
			}
		}

		delete patternBytes;
		delete patternMask;

		return moduleInfo.baseAddress + patternAddress;
	}
}