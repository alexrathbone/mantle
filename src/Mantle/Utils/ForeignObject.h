#pragma once

#include <cstdint>
#include <cstddef>

namespace Mantle
{
	class ForeignObject
	{
	public:
		ForeignObject(void* ptr = nullptr) : ptr(ptr)
		{
		}

		ForeignObject(uintptr_t ptr = 0) : ptr((void*) ptr)
		{
		}

		void* data()
		{
			return ptr;
		}

		const void* data() const
		{
			return ptr;
		}

		explicit operator bool() const
		{
			return ptr != nullptr;
		}

		template<typename T>
		T get(uintptr_t offset)
		{
			return *(T*) ((uintptr_t) ptr + offset);
		}

		template<typename T, typename... Ts>
		T call(uintptr_t offset, Ts... args)
		{
			return (
				(T(__thiscall*)(void*, Ts...))
				(*(uintptr_t*) ((*(uintptr_t*) ptr) + offset)))(ptr, args...);
		}

	private:
		void* ptr = nullptr;
	};

	inline bool operator==(const Mantle::ForeignObject& lhs, const Mantle::ForeignObject& rhs)
	{
		return lhs.data() == rhs.data();
	}

	inline bool operator==(const Mantle::ForeignObject& lhs, std::nullptr_t)
	{
		return lhs.data() == nullptr;
	}

	inline bool operator==(std::nullptr_t, const Mantle::ForeignObject& rhs)
	{
		return nullptr == rhs.data();
	}

	inline bool operator!=(const Mantle::ForeignObject& lhs, const Mantle::ForeignObject& rhs)
	{
		return lhs.data() != rhs.data();
	}

	inline bool operator!=(const Mantle::ForeignObject& lhs, std::nullptr_t)
	{
		return lhs.data() != nullptr;
	}

	inline bool operator!=(std::nullptr_t, const Mantle::ForeignObject& rhs)
	{
		return nullptr != rhs.data();
	}
}
