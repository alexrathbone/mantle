#pragma once

#include <string>
#include <vector>

#include <cstdint>

namespace Mantle
{
	class VtHookManager
	{
	public:
		VtHookManager(uintptr_t instance, uint16_t backupSize = 64);
		~VtHookManager();

		uintptr_t hook(uint16_t index, uintptr_t callback);
		void unhook(uint16_t index);

	private:
		uintptr_t instance;
		uintptr_t originalVt;
		std::vector<uintptr_t> fakeVt;
	};
}
