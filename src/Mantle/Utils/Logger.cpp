#include "Mantle/Utils/Logger.h"

#include "Mantle/Utils/Timestamp.h"

#include <fstream>
#include <iomanip>

Mantle::Logger* Mantle::Logger::Instance = nullptr;

namespace Mantle
{
	void Logger::Initialise(const std::string& filepath)
	{
		if (Instance != nullptr)
			return;

		Instance = new Mantle::Logger(filepath);
	}

	void Logger::Cleanup()
	{
		if (Instance == nullptr)
			return;

		delete Instance;
		Instance = nullptr;
	}

	Mantle::Logger* Logger::GetInstance()
	{
		return Mantle::Logger::Instance;
	}

	Logger::Logger(const std::string& filepath) :
		filepath(filepath), level(Mantle::LOGLEVEL_DEBUG)
	{
		std::ofstream f(filepath); // delete current file
	}

	Logger::~Logger()
	{
	}

	std::string Logger::getFilePath()
	{
		return filepath;
	}

	bool Logger::shouldShowMessage(const Mantle::LogLevel level)
	{
		return level >= this->level;
	}

	LogStream::LogStream(const Mantle::LogLevel level) :
		level(level)
	{
	}

	LogStream::~LogStream()
	{
		Mantle::Logger* logger = Mantle::Logger::GetInstance();

		if (logger->shouldShowMessage(level))
		{
			std::ofstream file(logger->getFilePath(), std::ios::app);
			
			std::string levelStr;

			switch (level)
			{
			case Mantle::LOGLEVEL_DEBUG:
				levelStr = "DEBUG";
				break;

			case Mantle::LOGLEVEL_INFO:
				levelStr = "INFO";
				break;

			case Mantle::LOGLEVEL_WARNING:
				levelStr = "WARNING";
				break;

			case Mantle::LOGLEVEL_ERROR:
				levelStr = "ERROR";
				break;

			case Mantle::LOGLEVEL_FATAL:
				levelStr = "FATAL";
				break;

			default:
				levelStr = "UNKNOWN";
			}

			Mantle::Timestamp timestamp;
			file << "[" << timestamp.toString() << "] " << "[" << std::setw(8) << levelStr << "] "
				<< this->str() << std::endl;
		}
	}
}
