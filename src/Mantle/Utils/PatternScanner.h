#pragma once

#include <string>

#include <cstdint>

namespace Mantle
{
	uintptr_t FindPattern(std::string pattern, const std::string& moduleName = "");
}
