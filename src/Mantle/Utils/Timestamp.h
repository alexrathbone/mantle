#pragma once

#include <string>

#include <ctime>

namespace Mantle
{
	class Timestamp
	{
	public:
		Timestamp();

		void update();

		std::string toString();

	private:
		std::time_t currentTime;

		int sec, min, hour;
		int day, month, year;
	};
}
