#include "Mantle/Utils/Timestamp.h"

#include <iostream>
#include <sstream>
#include <iomanip>

namespace Mantle
{
	Timestamp::Timestamp()
	{
		update();
	}

	void Timestamp::update()
	{
		std::tm* localTime;

		std::time(&currentTime);
		localTime = std::localtime(&currentTime);

		sec = localTime->tm_sec;
		min = localTime->tm_min;
		hour = localTime->tm_hour;
		day = localTime->tm_mday;
		month = localTime->tm_mon + 1;
		year = localTime->tm_year + 1900;
	}

	std::string Timestamp::toString()
	{
		std::stringstream ss;
		ss << year << "-" <<
			std::setfill('0') << std::setw(2) << month << "-" <<
			std::setfill('0') << std::setw(2) << day << " " <<
			std::setfill('0') << std::setw(2) << hour << ":" <<
			std::setfill('0') << std::setw(2) << min << ":" <<
			std::setfill('0') << std::setw(2) << sec;

		return ss.str();
	}
}
