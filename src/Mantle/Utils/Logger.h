#pragma once

#include <string>
#include <sstream>

namespace Mantle
{
	enum LogLevel
	{
		LOGLEVEL_DEBUG,
		LOGLEVEL_INFO,
		LOGLEVEL_WARNING,
		LOGLEVEL_ERROR,
		LOGLEVEL_FATAL
	};

	class LogStream : public std::stringstream
	{
	public:
		LogStream(const Mantle::LogLevel level);
		~LogStream();

		LogStream(const Mantle::LogStream&) = delete;
		void operator=(const Mantle::LogStream&) = delete;

	private:
		Mantle::LogLevel level;
	};

	class Logger
	{
	public:
		static void Initialise(const std::string& filepath);
		static void Cleanup();
		static Mantle::Logger* GetInstance();

		std::string getFilePath();
		bool shouldShowMessage(const Mantle::LogLevel level);

	private:
		static Mantle::Logger* Instance;

		Logger(const std::string& filepath);
		~Logger();

		std::string filepath;
		Mantle::LogLevel level;
	};

	inline Mantle::LogStream& LogDebug()
	{
		return Mantle::LogStream(Mantle::LOGLEVEL_DEBUG);
	}
}

#define Mantle_LogDebug() Mantle::LogStream(Mantle::LOGLEVEL_DEBUG)
#define Mantle_LogInfo() Mantle::LogStream(Mantle::LOGLEVEL_INFO)
#define Mantle_LogWarning() Mantle::LogStream(Mantle::LOGLEVEL_WARNING)
#define Mantle_LogError() Mantle::LogStream(Mantle::LOGLEVEL_ERROR)
#define Mantle_LogFatal() Mantle::LogStream(Mantle::LOGLEVEL_FATAL)
