#pragma once

#include <Windows.h>

#include <string>
#include <vector>

#include <cstdint>

namespace Mantle
{
	typedef HANDLE ProcessHandle;
	typedef HMODULE ModuleHandle;

	struct ModuleInfo
	{
		std::string name;
		std::string path;
		uintptr_t baseAddress;
		uintptr_t size;
	};

	std::vector<std::string> GetLoadedModulesNames();
	std::vector<Mantle::ModuleInfo> GetLoadedModules();
	Mantle::ModuleHandle GetModuleByName(const std::string& name = "");
	Mantle::ModuleInfo GetModuleInfo(Mantle::ModuleHandle module);
	uintptr_t FollowRelativeAddress(uintptr_t address, uintptr_t trail);
}
