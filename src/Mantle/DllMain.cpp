#include "Mantle/Context.h"
#include "Mantle/Utils/Logger.h"
#include "Mantle/Utils/VtHookManager.h"

#include <Windows.h>
#include <d3d9.h>

static Mantle::VtHookManager* D3D9Vt = nullptr;
static uintptr_t CreateDevice_Original = 0;

typedef IDirect3D9* (WINAPI* Direct3DCreate9_t)(UINT);
static HMODULE Direct3D9Module = nullptr;
static Direct3DCreate9_t Direct3DCreate9_Real = nullptr;

BOOL WINAPI DllMain(HINSTANCE module, DWORD reason, LPVOID reserved)
{
	switch (reason)
	{
	case DLL_PROCESS_ATTACH:
		Mantle::Logger::Initialise("mantle.log");
		break;

	case DLL_PROCESS_DETACH:
		if (D3D9Vt != nullptr)
			delete D3D9Vt;

		if (Direct3D9Module != nullptr)
			FreeLibrary(Direct3D9Module);

		Mantle::Logger::Cleanup();
		break;
	}

	return TRUE;
}

static HRESULT __stdcall CreateDevice_Hook(IDirect3D9* _this, UINT Adapter, D3DDEVTYPE DeviceType, HWND hFocusWindow, DWORD BehaviorFlags, D3DPRESENT_PARAMETERS* pPresentationParameters, IDirect3DDevice9** ppReturnedDeviceInterface)
{
	IDirect3DDevice9* device;
	HRESULT result = decltype(&CreateDevice_Hook)(CreateDevice_Original)
		(_this, Adapter, DeviceType, hFocusWindow, BehaviorFlags, pPresentationParameters, &device);

	Mantle_LogDebug() << "IDirect3DDevice9: 0x" << std::hex << (uintptr_t) device;

	Mantle::Context::Initialise(device);

	*ppReturnedDeviceInterface = device;
	return result;
}

IDirect3D9* WINAPI Direct3DCreate9(UINT SDKVersion)
{
	Mantle_LogInfo() << "Mantle v" << MANTLE_VERSION_STRING << " (" << MANTLE_BUILDTIMESTAMP << ")";

	if (Direct3D9Module == nullptr)
	{
		// Load system d3d9.dll
		char systemPath[MAX_PATH];
		GetSystemDirectory(systemPath, ARRAYSIZE(systemPath));

		std::stringstream ss;
		ss << systemPath << "\\d3d9.dll";
		std::string path = ss.str();

		Mantle_LogInfo() << "Using " << path;

		Direct3D9Module = LoadLibrary(path.c_str());

		if (Direct3D9Module == nullptr)
		{
			Mantle_LogFatal() << "Failed to load system d3d9.dll";
			return nullptr;
		}
		else
		{
			Direct3DCreate9_Real = (Direct3DCreate9_t) GetProcAddress(Direct3D9Module, "Direct3DCreate9");
		}
	}

	if (Direct3DCreate9_Real == nullptr)
	{
		Mantle_LogFatal() << "Failed to locate Direct3DCreate9";
		return nullptr;
	}
	else
	{
		IDirect3D9* d3d9 = Direct3DCreate9_Real(SDKVersion);
		
		Mantle_LogDebug() << "IDirect3D9: 0x" << std::hex << (uintptr_t) d3d9;

		D3D9Vt = new Mantle::VtHookManager((uintptr_t) d3d9);
		CreateDevice_Original = D3D9Vt->hook(16, (uintptr_t) CreateDevice_Hook);

		return d3d9;
	}
}
