#pragma once

#include <string>

#include <cstdint>

struct IDirect3DDevice9;
struct ID3DXFont;

namespace Mantle
{
	class Font
	{
	public:
		Font(IDirect3DDevice9* device, const std::string& facename, int size, bool bold = false);
		~Font();

		void onLostDevice();
		void onResetDevice();

		void drawText(const std::string& text, int x, int y, int32_t color = 0);
		void getSize(const std::string& text, int& width, int& height);

	private:
		ID3DXFont* d3dxFont;

		int32_t color;
	};
}
