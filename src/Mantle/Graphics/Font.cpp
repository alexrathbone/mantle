#include "Mantle/Graphics/Font.h"

#include "Mantle/Exception.h"

#include <d3dx9.h>

namespace Mantle
{
	Font::Font(IDirect3DDevice9* device, const std::string& facename, int size, bool bold)
	{
		HRESULT result = D3DXCreateFont(
			device, 
			size, 
			0, 
			bold ? FW_BOLD : FW_NORMAL,
			1, 
			false, 
			DEFAULT_CHARSET, 
			OUT_DEFAULT_PRECIS, 
			ANTIALIASED_QUALITY, 
			DEFAULT_PITCH | FF_DONTCARE, 
			facename.c_str(), 
			&d3dxFont);

		if (FAILED(result))
		{
			throw Mantle::Exception("D3DXCreateFont failed");
		}
	}

	Font::~Font()
	{
		d3dxFont->Release();
	}

	void Font::onLostDevice()
	{
		d3dxFont->OnLostDevice();
	}

	void Font::onResetDevice()
	{
		d3dxFont->OnResetDevice();
	}

	void Font::drawText(const std::string& text, int x, int y, int32_t color)
	{
		RECT textSize = { 0, 0, 0, 0 };
		d3dxFont->DrawTextA(NULL, text.c_str(), -1, &textSize, DT_CALCRECT, (D3DCOLOR) color);

		RECT rect = { 0, 0, 0, 0 };
		rect.left = x;
		rect.top = y;
		rect.right = x + textSize.right;
		rect.bottom = y + textSize.bottom;

		d3dxFont->DrawTextA(NULL, text.c_str(), -1, &rect, NULL, (D3DCOLOR) color);
	}

	void Font::getSize(const std::string& text, int& width, int& height)
	{
		RECT textSize = { 0, 0, 0, 0 };
		d3dxFont->DrawTextA(NULL, text.c_str(), -1, &textSize, DT_CALCRECT, (D3DCOLOR) 0xFFFFFFFF);

		width = textSize.right;
		height = textSize.bottom;
	}
}