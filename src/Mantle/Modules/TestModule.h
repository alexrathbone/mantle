#pragma once

#include "Mantle/Module.h"
#include "Mantle/GW2/Array.h"
#include "Mantle/GW2/Agent.h"
#include "Mantle/GW2/Character.h"

#include <string>
#include <sstream>
#include <vector>

#include <cstdint>

namespace Mantle
{
	class TestModule : public Mantle::Module
	{
	public:
		TestModule(Mantle::Context* context) : Module(context)
		{
		}

		void onLoad();
		void onShutdown();
		void onLostDevice();
		void onResetDevice();
		void onRender();

	private:
		std::stringstream ss;

		Mantle::GW2::Array<uintptr_t> gw2Agents, gw2Characters;
		std::vector<Mantle::GW2::Agent> agents;
		std::vector<Mantle::GW2::Character> characters;
	};
}
