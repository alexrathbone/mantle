#include "Mantle/Modules/TestModule.h"

#include "Mantle/Context.h"
#include "Mantle/Graphics/Font.h"
#include "Mantle/GW2/Offsets.h"
#include "Mantle/Utils/ForeignObject.h"
#include "Mantle/Utils/Logger.h"

namespace Mantle
{
	void TestModule::onLoad()
	{
	}

	void TestModule::onShutdown()
	{
	}

	void TestModule::onLostDevice()
	{

	}

	void TestModule::onResetDevice()
	{
	}

	void TestModule::onRender()
	{
		if (!context->isMapOpen())
		{
			context->getDefaultFont(true)->drawText("Mantle v0.0.1", 1300, 10, 0xFFFFFFFF);
			context->getDefaultFont()->drawText("FPS: " + std::to_string(context->getFps()), 1300, 26, 0xFFFFFFFF);
			context->getDefaultFont()->drawText("Ping: " + std::to_string(context->getPing()) + "ms", 1300, 42, 0xFFFFFFFF);

			Mantle::ForeignObject chcliContext = context->getCharClientContext();
			Mantle::ForeignObject asContext = context->getAgentSelectionContext();
			Mantle::ForeignObject avContext = context->getAgentViewContext();
			Mantle::ForeignObject wvContext = context->getWorldViewContext();

			if (chcliContext && asContext && avContext && wvContext && wvContext.get<uint8_t>(Mantle::GW2::Offsets::WvContextCamStatus))
			{
				// Update agents array
				gw2Agents = avContext.get<Mantle::GW2::Array<uintptr_t>>(Mantle::GW2::Offsets::AvContextAgentArray);
				agents.clear();

				for (uint32_t i = 0; i < gw2Agents.size; ++i)
				{
					Mantle::ForeignObject avAgentPointer = gw2Agents.basePointer[i];

					if (avAgentPointer)
					{
						Mantle::ForeignObject agentPointer = avAgentPointer.call<void*>(Mantle::GW2::Offsets::AvAgentVtGetAgent);

						if (agentPointer)
						{
							agents.push_back(Mantle::GW2::Agent(agentPointer));
						}
					}
				}
				
				// Update characters array
				gw2Characters = chcliContext.get<Mantle::GW2::Array<uintptr_t>>(Mantle::GW2::Offsets::ChCliCharArray);
				characters.clear();

				for (uint32_t i = 0; i < gw2Characters.size; ++i)
				{
					Mantle::ForeignObject characterPointer = gw2Characters.basePointer[i];

					if (characterPointer)
					{
						characters.push_back(Mantle::GW2::Character(characterPointer));
					}
				}

				Mantle::ForeignObject targetAgentPointer = asContext.get<void*>(Mantle::GW2::Offsets::AsContextLockedSelection);

				if (targetAgentPointer)
				{
					Mantle::GW2::Agent targetAgent(targetAgentPointer);

					if (targetAgent.getType() == Mantle::GW2::AGENT_TYPE_CHARACTER)
					{
						uint32_t targetAgentId = targetAgent.getAgentId();

						for (auto it = characters.begin(); it != characters.end(); ++it)
						{
							if (targetAgentId == (*it).getAgentId()) // Found target character
							{
								Mantle::GW2::Health health = (*it).getHealth();

								ss.precision(0);
								ss << std::fixed << health.currentHealth << "/" << health.maxHealth;
								context->getDefaultFont()->drawText(ss.str(), 785, 85, 0xFFFFFFFF);
								ss.str("");
								ss.clear();
							}
						}
					}
				}
			}
		}
	}
}
