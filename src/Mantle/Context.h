#pragma once

#include "Mantle/Version.h"
#include "Mantle/GW2/Pointers.h"

#include <d3d9.h>

#include <vector>

#include <cstdint>

namespace Mantle
{
	class VtHookManager;
	class Font;
	class Module;

	class Context
	{
	public:
		static void Initialise(IDirect3DDevice9* device);
		static void Cleanup();
		static Mantle::Context* GetInstance();

		IDirect3DDevice9* getDevice();
		intptr_t getOriginalWndProc();

		LRESULT wndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
		HRESULT deviceReset(D3DPRESENT_PARAMETERS* pPresentationParameters);
		HRESULT devicePresent(CONST RECT* pSourceRect, CONST RECT* pDestRect, HWND hDestWindowOverride, CONST RGNDATA* pDirtyRegion);

		uint32_t getFps();
		uint32_t getPing();
		bool isMapOpen();
		uintptr_t getCharClientContext();
		uintptr_t getAgentSelectionContext();
		uintptr_t getAgentViewContext();
		uintptr_t getWorldViewContext();

		Mantle::Font* getDefaultFont(bool bold = false);

		void loadModule(Mantle::Module* module);
		void unloadModule(Mantle::Module* module);

	private:
		static Mantle::Context* Instance;

		Context(IDirect3DDevice9* device);
		~Context();

		void scanForPointers();

		IDirect3DDevice9* device;
		HWND hwnd;

		intptr_t wndProcOriginal;
		Mantle::VtHookManager* deviceVt;
		uintptr_t deviceResetOriginal;
		uintptr_t devicePresentOriginal;

		Mantle::GW2::Pointers gw2Pointers;

		uint32_t fps, ping;
		bool mapOpen;

		Mantle::Font* tahomaBold;
		Mantle::Font* tahoma;

		std::vector<Mantle::Module*> loadedModules;
	};
}
