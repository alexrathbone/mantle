#pragma once

namespace Mantle
{
	class Context;

	class Module
	{
	public:
		Module(Mantle::Context* context) : context(context)
		{
		}

		virtual void onLoad() = 0;
		virtual void onShutdown() = 0;
		virtual void onLostDevice() = 0;
		virtual void onResetDevice() = 0;
		virtual void onRender() = 0;

	protected:
		Mantle::Context* context;
	};
}
