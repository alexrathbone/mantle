#include "Mantle/GW2/Character.h"

#include "Mantle/Exception.h"
#include "Mantle/GW2/Offsets.h"

namespace Mantle
{
	namespace GW2
	{
		Character::Character(Mantle::ForeignObject pointer) : pointer(pointer)
		{
			if (pointer == nullptr)
				throw Mantle::Exception("Character pointer invalid");
		}

		bool Character::isPlayer()
		{
			return pointer.call<bool>(Mantle::GW2::Offsets::CharVtIsPlayer);
		}

		bool Character::isDowned()
		{
			return pointer.call<bool>(Mantle::GW2::Offsets::CharVtIsDowned);
		}

		bool Character::isInWater()
		{
			return pointer.call<bool>(Mantle::GW2::Offsets::CharVtIsInWater);
		}

		Mantle::ForeignObject Character::getAgent()
		{
			return pointer.call<void*>(Mantle::GW2::Offsets::CharVtGetAgent);
		}

		Mantle::ForeignObject Character::getPlayer()
		{
			if (isPlayer())
				return pointer.call<void*>(Mantle::GW2::Offsets::CharVtGetPlayer);
			else
				return nullptr;
		}

		Mantle::ForeignObject Character::getCombatant()
		{
			return pointer.call<void*>(Mantle::GW2::Offsets::CharVtGetCombatant);
		}

		uint32_t Character::getAgentId()
		{
			return pointer.call<uint32_t>(Mantle::GW2::Offsets::CharVtGetAgentId);
		}

		Mantle::GW2::Health Character::getHealth()
		{
			Mantle::GW2::Health health;
			Mantle::ForeignObject healthPointer = pointer.get<void*>(Mantle::GW2::Offsets::CharHealth);

			health.currentHealth = healthPointer.get<float>(Mantle::GW2::Offsets::HealthCurrent);
			health.maxHealth = healthPointer.get<float>(Mantle::GW2::Offsets::HealthMax);

			return health;
		}

		Mantle::GW2::CoreStats Character::getCoreStats()
		{
			Mantle::GW2::CoreStats stats;
			Mantle::ForeignObject statsPointer = pointer.call<void*>(Mantle::GW2::Offsets::CharVtGetCoreStats);

			stats.power = statsPointer.get<uint32_t>(Mantle::GW2::Offsets::CoreStatsStats + 0x4 * 0x1);
			stats.precision = statsPointer.get<uint32_t>(Mantle::GW2::Offsets::CoreStatsStats + 0x4 * 0x2);
			stats.toughness = statsPointer.get<uint32_t>(Mantle::GW2::Offsets::CoreStatsStats + 0x4 * 0x3);
			stats.vitality = statsPointer.get<uint32_t>(Mantle::GW2::Offsets::CoreStatsStats + 0x4 * 0x4);
			stats.ferocity = statsPointer.get<uint32_t>(Mantle::GW2::Offsets::CoreStatsStats + 0x4 * 0x5);
			stats.healingPower = statsPointer.get<uint32_t>(Mantle::GW2::Offsets::CoreStatsStats + 0x4 * 0x6);
			stats.conditionDamage = statsPointer.get<uint32_t>(Mantle::GW2::Offsets::CoreStatsStats + 0x4 * 0x7);
			stats.concentration = statsPointer.get<uint32_t>(Mantle::GW2::Offsets::CoreStatsStats + 0x4 * 0x8);
			stats.expertise = statsPointer.get<uint32_t>(Mantle::GW2::Offsets::CoreStatsStats + 0x4 * 0x9);
			stats.agonyResistance = statsPointer.get<uint32_t>(Mantle::GW2::Offsets::CoreStatsStats + 0x4 * 0xA);

			return stats;
		}
	}
}
