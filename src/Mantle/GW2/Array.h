#pragma once

#include <cstdint>

namespace Mantle
{
	namespace GW2
	{
		template<typename T>
		struct Array
		{
			T* basePointer;
			uint32_t capacity;
			uint32_t size;

			bool isValid()
			{
				return !!basePointer;
			}
		};
	}
}
