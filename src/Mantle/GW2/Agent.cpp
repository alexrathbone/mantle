#include "Mantle/GW2/Agent.h"

#include "Mantle/Exception.h"
#include "Mantle/GW2/Offsets.h"

namespace Mantle
{
	namespace GW2
	{
		Agent::Agent(Mantle::ForeignObject pointer) : pointer(pointer)
		{
			if (pointer == nullptr)
				throw Mantle::Exception("Agent pointer invalid");
		}

		uint32_t Agent::getAgentId()
		{
			return pointer.call<uint32_t>(Mantle::GW2::Offsets::AgentVtGetAgentId);
		}

		Mantle::GW2::AgentType Agent::getType()
		{
			uint32_t type = pointer.call<uint32_t>(Mantle::GW2::Offsets::AgentVtGetType);

			switch (type)
			{
			case Mantle::GW2::AGENT_TYPE_CHARACTER:
				return Mantle::GW2::AGENT_TYPE_CHARACTER;

			case Mantle::GW2::AGENT_TYPE_GADGET:
				return Mantle::GW2::AGENT_TYPE_GADGET;

			case Mantle::GW2::AGENT_TYPE_ITEM:
				return Mantle::GW2::AGENT_TYPE_ITEM;

			default:
				return Mantle::GW2::AGENT_TYPE_UNKNOWN;
			}
		}
	}
}
