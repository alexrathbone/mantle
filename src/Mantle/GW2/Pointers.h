#pragma once

#include <cstdint>

namespace Mantle
{
	namespace GW2
	{
		struct Pointers
		{
			uintptr_t fps; // uint32_t
			uintptr_t ping; // uint32_t
			uintptr_t mapOpen; // uint32_t

			uintptr_t charClientContext; // CharClientContext
			uintptr_t agentSelectionContext; // AgentSelectionContext
			uintptr_t agentViewContext; // AgentViewContext
			uintptr_t worldViewContext; // WorldViewContext
		};
	}
}
