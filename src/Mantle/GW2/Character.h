#pragma once

#include "Mantle/Utils/ForeignObject.h"

namespace Mantle
{
	namespace GW2
	{
		struct Health
		{
			float currentHealth;
			float maxHealth;
		};

		struct CoreStats
		{
			uint32_t power;
			uint32_t precision;
			uint32_t toughness;
			uint32_t vitality;
			uint32_t ferocity;
			uint32_t healingPower;
			uint32_t conditionDamage;
			uint32_t concentration;
			uint32_t expertise;
			uint32_t agonyResistance;
		};

		class Character
		{
		public:
			Character(Mantle::ForeignObject pointer);

			bool isPlayer();
			bool isDowned();
			bool isInWater();

			Mantle::ForeignObject getAgent();
			Mantle::ForeignObject getPlayer();
			Mantle::ForeignObject getCombatant();

			uint32_t getAgentId();
			Mantle::GW2::Health getHealth();
			Mantle::GW2::CoreStats getCoreStats();

		private:
			Mantle::ForeignObject pointer;
		};
	}
}
