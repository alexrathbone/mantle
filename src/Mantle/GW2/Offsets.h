#pragma once

#include <cstdint>

namespace Mantle
{
	namespace GW2
	{
		namespace Offsets
		{
			// GW2::CharClientContext
			const uintptr_t ChCliVtGetControlledPlayer = 0x50; // GW2::Player*, "CharClientContext()->GetControlledPlayer()"
			const uintptr_t ChCliVtGetOwnedCharacter = 0x68; // GW2::Character*, "CharClientContext()->GetOwnedCharacter()"

			const uintptr_t ChCliCharArray = 0x58; // GW2::Array<GW2::Character*>, pointer followed by 2 uint32s
			const uintptr_t ChCliPlayerArray = 0x78; // GW2::Array<GW2::Player*>, pointer followed by 2 uint32s

			// GW2::AgentSelectionContext
			const uintptr_t AsContextHoverSelection = 0xF8; // GW2::Agent*, "!m_hoverSelection"
			const uintptr_t AsContextLockedSelection = 0x230; // GW2::Agent*, "!m_lockedSelection"

			// GW2::AgentViewContext
			const uintptr_t AvContextAgentArray = 0x70; // GW2::Array<GW2::AvAgent*>, pointer followed by 2 uint32s

			// GW2::WorldViewContext
			const uintptr_t WvContextCamStatus = 0x12A; // uint8_t, bit inside WorldViewContext that is 0 when loading

			// GW2::AgentViewAgent
			const uintptr_t AvAgentVtGetAgent = 0x60; // GW2::Agent*, "wmAgent->GetAgent() != agent"

			// GW2::Agent
			const uintptr_t AgentVtGetAgentId = 0xF8; // uint32_t, "targetAgent->GetAgentId()"
			const uintptr_t AgentVtGetType = 0x180; // GW2::AgentType, "m_agent->GetType()"

			// GW2::Character
			const uintptr_t CharVtGetAgent = 0x0; // GW2::Agent*, "character->GetAgent()"
			const uintptr_t CharVtGetCoreStats = 0x28; // GW2::CoreStats, "character->GetCoreStats()"
			const uintptr_t CharVtGetCombatant = 0x118; // GW2::Combatant*, "m_combatant"
			const uintptr_t CharVtGetAgentId = 0x198; // uint32_t, "m_agent->GetAgentId() == character->GetAgentId()"
			const uintptr_t CharVtGetPlayer = 0x288; // GW2::Player*, "m_character->GetPlayer()"
			const uintptr_t CharVtIsDowned = 0x350; // bool, "character->IsDowned()"
			const uintptr_t CharVtIsInWater = 0x3C0; // bool, "character->IsInWater()"
			const uintptr_t CharVtIsPlayer = 0x450; // bool, "m_ownerCharacter->IsPlayer()"

			const uintptr_t CharHealth = 0x358; // GW2::Health, "m_health"

			// GW2::Player
			const uintptr_t PlayerVtGetCharOwned = 0x20; // GW2:Character*, "m_player->GetCharOwned()"
			const uintptr_t PlayerVtGetName = 0x78; // wchar_t[19], "m_character->GetPlayer()->GetName()"
			const uintptr_t PlayerVtGetPlayerId = 0x88; // uint32_t, "CharClientContext()->GetControlledPlayer()->GetPlayerId()"
			const uintptr_t PlayerVtIsLocalPlayer = 0xD8; // bool, "player->IsLocalPlayer()"

			// GW2::Health
			const uintptr_t HealthCurrent = 0xC; // float
			const uintptr_t HealthMax = 0x10; // float

			// GW2::CoreStats
			const uintptr_t CoreStatsStats = 0xA8;

			// GW2::Combatant
			const uintptr_t CombatantVtGetChar = 0x48; // GW2::Character, "combatant->GetChar()"
			const uintptr_t CombatantVtGetCombatantType = 0x50; // GW2::CombatantType, "combatant->GetCombatantType()"
		}
	}
}
