#include "Mantle/GW2/Player.h"

#include "Mantle/Exception.h"
#include "Mantle/GW2/Offsets.h"

#include <codecvt>

namespace Mantle
{
	namespace GW2
	{
		Player::Player(Mantle::ForeignObject pointer) : pointer(pointer)
		{
			if (pointer == nullptr)
				throw Mantle::Exception("Player pointer invalid");
		}

		bool Player::isLocalPlayer()
		{
			return pointer.call<bool>(Mantle::GW2::Offsets::PlayerVtIsLocalPlayer);
		}

		Mantle::ForeignObject Player::getCharOwned()
		{
			return pointer.call<void*>(Mantle::GW2::Offsets::PlayerVtGetCharOwned);
		}

		uint32_t Player::getPlayerId()
		{
			return pointer.call<uint32_t>(Mantle::GW2::Offsets::PlayerVtGetPlayerId);
		}

		std::string Player::getName()
		{
			void* name = pointer.call<void*>(Mantle::GW2::Offsets::PlayerVtGetName);
			wchar_t nameWide[19];
			memcpy(nameWide, name, 19 * 2);
			std::wstring_convert<std::codecvt_utf8<wchar_t>> conv;
			return conv.to_bytes(nameWide);
		}
	}
}
