#pragma once

#include "Mantle/Utils/ForeignObject.h"

#include <string>

namespace Mantle
{
	namespace GW2
	{
		class Player
		{
		public:
			Player(Mantle::ForeignObject pointer);

			bool isLocalPlayer();

			Mantle::ForeignObject getCharOwned();

			uint32_t getPlayerId();
			std::string getName();

		private:
			Mantle::ForeignObject pointer;
		};
	}
}
