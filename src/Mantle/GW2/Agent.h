#pragma once

#include "Mantle/Utils/ForeignObject.h"

namespace Mantle
{
	namespace GW2
	{
		enum AgentType
		{
			AGENT_TYPE_CHARACTER = 0,
			AGENT_TYPE_GADGET = 10,
			AGENT_TYPE_ITEM = 15,
			AGENT_TYPE_UNKNOWN = 999,
		};

		class Agent
		{
		public:
			Agent(Mantle::ForeignObject pointer);

			uint32_t getAgentId();
			Mantle::GW2::AgentType getType();

		private:
			Mantle::ForeignObject pointer;
		};
	}
}
