#include "Mantle/GW2/Combatant.h"

#include "Mantle/Exception.h"
#include "Mantle/GW2/Offsets.h"

namespace Mantle
{
	namespace GW2
	{
		Combatant::Combatant(Mantle::ForeignObject pointer) : pointer(pointer)
		{
			if (pointer == nullptr)
				throw Mantle::Exception("Combatant pointer invalid");
		}

		Mantle::ForeignObject Combatant::getChar()
		{
			return pointer.call<void*>(Mantle::GW2::Offsets::CombatantVtGetChar);
		}

		uint32_t Combatant::getCombatantType()
		{
			return pointer.call<uint32_t>(Mantle::GW2::Offsets::CombatantVtGetCombatantType);
		}
	}
}
