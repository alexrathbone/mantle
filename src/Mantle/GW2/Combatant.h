#pragma once

#include "Mantle/Utils/ForeignObject.h"

namespace Mantle
{
	namespace GW2
	{
		class Combatant
		{
		public:
			Combatant(Mantle::ForeignObject pointer);
			
			Mantle::ForeignObject getChar();

			uint32_t getCombatantType();

		private:
			Mantle::ForeignObject pointer;
		};
	}
}
