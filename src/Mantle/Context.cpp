#include "Mantle/Context.h"

#include "Mantle/Exception.h"
#include "Mantle/Module.h"
#include "Mantle/Graphics/Font.h"
#include "Mantle/Modules/TestModule.h"
#include "Mantle/Utils/Logger.h"
#include "Mantle/Utils/VtHookManager.h"
#include "Mantle/Utils/PatternScanner.h"
#include "Mantle/Utils/Memory.h"

#include <Windows.h>

#include <algorithm>

Mantle::Context* Mantle::Context::Instance = nullptr;

static LRESULT WndProc_Hook(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	if (msg == WM_DESTROY)
	{
		Mantle_LogDebug() << "WM_DESTROY";

		intptr_t original = Mantle::Context::GetInstance()->getOriginalWndProc();
		Mantle::Context::Cleanup();
		return CallWindowProc((WNDPROC) original, hwnd, msg, wParam, lParam);
	}
	else
		return Mantle::Context::GetInstance()->wndProc(hwnd, msg, wParam, lParam);
}

static HRESULT __stdcall Reset_Hook(IDirect3DDevice9* _this, D3DPRESENT_PARAMETERS* pPresentationParameters)
{
	return Mantle::Context::GetInstance()->deviceReset(pPresentationParameters);
}

static HRESULT __stdcall Present_Hook(IDirect3DDevice9* _this, CONST RECT* pSourceRect, CONST RECT* pDestRect, HWND hDestWindowOverride, CONST RGNDATA* pDirtyRegion)
{
	return Mantle::Context::GetInstance()->devicePresent(pSourceRect, pDestRect, hDestWindowOverride, pDirtyRegion);
}

namespace Mantle
{
	void Context::Initialise(IDirect3DDevice9* device)
	{
		if (Instance != nullptr)
			return;

		Instance = new Mantle::Context(device);
	}

	void Context::Cleanup()
	{
		if (Instance == nullptr)
			return;

		delete Instance;
		Instance = nullptr;
	}

	Mantle::Context* Context::GetInstance()
	{
		return Instance;
	}

	Context::Context(IDirect3DDevice9* device) :
		device(device)
	{
		Mantle_LogDebug() << "Context::Context()";

		D3DDEVICE_CREATION_PARAMETERS params;
		device->GetCreationParameters(&params);
		hwnd = GetActiveWindow();

		wndProcOriginal = SetWindowLongPtr(hwnd, GWLP_WNDPROC, (LONG_PTR) WndProc_Hook);

		deviceVt = new Mantle::VtHookManager((uintptr_t) device, 200);
		deviceResetOriginal = deviceVt->hook(16, (uintptr_t) Reset_Hook);
		devicePresentOriginal = deviceVt->hook(17, (uintptr_t) Present_Hook);

		scanForPointers();

		Mantle_LogDebug() << "";
		Mantle_LogDebug() << "Pointers";
		Mantle_LogDebug() << "--";
		Mantle_LogDebug() << "CharClientContext: 0x" << std::hex << gw2Pointers.charClientContext;
		Mantle_LogDebug() << "AgentSelectionContext: 0x" << std::hex << gw2Pointers.agentSelectionContext;
		Mantle_LogDebug() << "AgentViewContext: 0x" << std::hex << gw2Pointers.agentViewContext;
		Mantle_LogDebug() << "WorldViewContext: 0x" << std::hex << gw2Pointers.worldViewContext;
		Mantle_LogDebug() << "";

		tahomaBold = new Mantle::Font(device, "Tahoma", 16, true);
		tahoma = new Mantle::Font(device, "Tahoma", 16, false);

		loadModule(new Mantle::TestModule(this));
	}

	Context::~Context()
	{
		Mantle_LogDebug() << "Context::~Context()";

		for (auto it = loadedModules.begin(); it != loadedModules.end(); ++it)
		{
			(*it)->onShutdown();
			delete (*it);
		}

		loadedModules.clear();

		delete tahomaBold;
		delete deviceVt;
		SetWindowLongPtr(hwnd, GWLP_WNDPROC, wndProcOriginal);
	}

	IDirect3DDevice9* Context::getDevice()
	{
		return device;
	}

	intptr_t Context::getOriginalWndProc()
	{
		return wndProcOriginal;
	}

	LRESULT Context::wndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		return CallWindowProc((WNDPROC) wndProcOriginal, hwnd, msg, wParam, lParam);
	}

	HRESULT Context::deviceReset(D3DPRESENT_PARAMETERS* pPresentationParameters)
	{
		Mantle_LogDebug() << "Direct3D9 device reset (" << pPresentationParameters->BackBufferWidth << "x" << pPresentationParameters->BackBufferHeight << ")";

		tahomaBold->onLostDevice();
		tahoma->onLostDevice();

		for (auto it = loadedModules.begin(); it != loadedModules.end(); ++it)
		{
			(*it)->onLostDevice();
		}

		auto original = decltype(&Reset_Hook)(deviceResetOriginal);
		HRESULT res = original(device, pPresentationParameters);

		tahomaBold->onResetDevice();
		tahoma->onResetDevice();

		for (auto it = loadedModules.begin(); it != loadedModules.end(); ++it)
		{
			(*it)->onResetDevice();
		}

		return res;
	}

	HRESULT Context::devicePresent(CONST RECT* pSourceRect, CONST RECT* pDestRect, HWND hDestWindowOverride, CONST RGNDATA* pDirtyRegion)
	{
		device->BeginScene();

		// Update GW2 data
		fps = *(uint32_t*) gw2Pointers.fps;
		ping = *(uint32_t*) gw2Pointers.ping;
		mapOpen = *(uint32_t*) gw2Pointers.mapOpen == 1;

		for (auto it = loadedModules.begin(); it != loadedModules.end(); ++it)
		{
			(*it)->onRender();
		}

		device->EndScene();

		auto original = decltype(&Present_Hook)(devicePresentOriginal);
		return original(device, pSourceRect, pDestRect, hDestWindowOverride, pDirtyRegion);
	}

	void Context::scanForPointers()
	{
		Mantle::ModuleInfo moduleInfo = Mantle::GetModuleInfo(Mantle::GetModuleByName("gw2-64.exe"));
		uintptr_t gw2Base = moduleInfo.baseAddress;
		uintptr_t pattern = 0x0;

		// FPS
		/*
		 * Find address of current FPS (swap between 30 and 60FPS framelock to make it easier)
		 * and monitor what writes to current address
		 * 89 0D BBCE1A01  - mov [Gw2-64.exe+200DB18], ecx
		 * Strip out the relative address bytes to make the pattern
		 */
		pattern = Mantle::FindPattern("CC 83 0D ?? ?? ?? ?? 20 89 0D ?? ?? ?? ?? C3 CC CC", "gw2-64.exe");

		if (pattern == 0)
			throw Mantle::Exception("FPS pattern not found");
		else
		{
			pattern += 0x8;
			gw2Pointers.fps = Mantle::FollowRelativeAddress(pattern + 0x2, 0x0);
		}

		// Ping
		/*
		 * Find address of current ping and check what writes to the address
		 * Look for an instruction similar to: 89 1D C170A201  - mov [Gw2-64.exe+1F951F0], ebx <<
		 */
		pattern = Mantle::FindPattern("CC 40 53 48 83 EC 30 8B D9 81 F9 ?? ?? ?? ?? 77 1F", "gw2-64.exe");

		if (pattern == 0)
			throw Mantle::Exception("Ping pattern not found");
		else
		{
			pattern += 0x2A;
			gw2Pointers.ping = Mantle::FollowRelativeAddress(pattern + 0x2, 0x0);
		}

		// Map Open
		/*
		 * Search for a 4 byte int with a value of 1 (make sure map is open), then close it and search for 0.
		 * Keep toggling until you're left with a few values
		 */
		pattern = Mantle::FindPattern("B8 11 00 00 00 E9 ?? ?? 00 00 83 3D ?? ?? ?? ?? 00 74 0A", "gw2-64.exe");

		if (pattern == 0)
			throw Mantle::Exception("Map Open pattern not found");
		else
		{
			pattern += 0xA;
			gw2Pointers.mapOpen = Mantle::FollowRelativeAddress(pattern + 0x2, 0x1);
		}

		// CharClientContext
		/*
		 * Search for "CharClientContext()->GetOwnedCharacter() == character" and look at the first call
		 * (there should be 2 above, one to a relative address and one to r8+0x68)
		 */
		pattern = Mantle::FindPattern("E8 ?? ?? ?? ?? 48 8B C8 4C 8B 00 41 FF 50 68 48 3B C3", "gw2-64.exe");

		if (pattern == 0)
			throw Mantle::Exception("CharClientContext() pattern not found");
		else
		{
			auto CharClientContext = (void*(*)(void)) Mantle::FollowRelativeAddress(pattern + 0x1, 0x0);
			gw2Pointers.charClientContext = (uintptr_t) CharClientContext();
		}

		// AgentSelectionContext
		/*
		 * Search for "ViewAdvanceAgentSelect" and look at the second call below
		 */
		pattern = Mantle::FindPattern("E8 ?? ?? ?? ?? 0F 28 CE 48 8B C8 48 8B 10 FF 92 F0 00 00 00");

		if (pattern == 0)
			throw Mantle::Exception("AgentSelectionContext pattern not found");
		else
		{
			gw2Pointers.agentSelectionContext = Mantle::FollowRelativeAddress(Mantle::FollowRelativeAddress(pattern + 0x1, 0x0) + 0x3, 0x0);
		}

		// AgentViewContext
		/*
		 * Search for "ViewAdvanceAgentView" and look at the second call below
		 */
		pattern = Mantle::FindPattern("E8 ?? ?? ?? ?? 0F 28 CE 48 8B C8 48 8B 10 FF 52 68 E8 ?? ?? ?? ??");

		if (pattern == 0)
			throw Mantle::Exception("AgentViewContext pattern not found");
		else
		{
			gw2Pointers.agentViewContext = Mantle::FollowRelativeAddress(Mantle::FollowRelativeAddress(pattern + 0x1, 0x0) + 0x3, 0x0);
		}

		// WorldViewContext
		/*
		 * Search for "ViewAdvanceWorldView" and look at the second call below
		 */
		pattern = Mantle::FindPattern("E8 ?? ?? ?? ?? 41 0F 28 D0 0F 28 CE 48 8B C8 48 8B 10 FF 52 08");

		if (pattern == 0)
			throw Mantle::Exception("WorldViewContext pattern not found");
		else
		{
			gw2Pointers.worldViewContext = Mantle::FollowRelativeAddress(Mantle::FollowRelativeAddress(pattern + 0x1, 0x0) + 0x7, 0x0);
		}
	}

	uint32_t Context::getFps()
	{
		return fps;
	}

	uint32_t Context::getPing()
	{
		return ping;
	}

	bool Context::isMapOpen()
	{
		return mapOpen;
	}

	uintptr_t Context::getCharClientContext()
	{
		return gw2Pointers.charClientContext;
	}

	uintptr_t Context::getAgentSelectionContext()
	{
		return gw2Pointers.agentSelectionContext;
	}

	uintptr_t Context::getAgentViewContext()
	{
		return gw2Pointers.agentViewContext;
	}

	uintptr_t Context::getWorldViewContext()
	{
		return gw2Pointers.worldViewContext;
	}

	Mantle::Font* Context::getDefaultFont(bool bold)
	{
		if (bold)
			return tahomaBold;
		else
			return tahoma;
	}

	void Context::loadModule(Mantle::Module* module)
	{
		if (std::find(loadedModules.begin(), loadedModules.end(), module) != loadedModules.end())
			return;

		module->onLoad();
		loadedModules.push_back(module);
	}

	void Context::unloadModule(Mantle::Module* module)
	{
		auto it = std::find(loadedModules.begin(), loadedModules.end(), module);

		if (it != loadedModules.end())
		{
			Mantle::Module* module = *it;
			loadedModules.erase(it);
			module->onShutdown();
			delete module;
		}
	}
}
